# frozen_string_literal: true

Sidekiq::Cron::Job.create(
  name: 'Schedule polling workers - every 1 min',
  cron: '* * * * *',
  class: 'SchedulePollingWorker'
)
