# frozen_string_literal: true

class Config
  class << self
    def api_host
      @api_host ||= ENV.fetch('LIBRARY_API_URL')
    end

    def api_username
      @api_username ||= ENV.fetch('LIBRARY_API_USERNAME')
    end

    def api_password
      @api_password ||= ENV.fetch('LIBRARY_API_PASSWORD')
    end

    def ca_file_path
      @ca_file_path ||= ENV['LIBRARY_CA_FILE_PATH'].presence
    end

    def redis_url
      @redis_url ||= ENV.fetch('REDIS_URL')
    end

    def request_operations_path
      '/api/v1/calibre/operations/request'
    end

    def update_operation_path(id)
      "/api/v1/calibre/operations/#{id.to_i}/update"
    end

    def upload_operation_files_path(id)
      "/api/v1/calibre/operations/#{id.to_i}/upload"
    end

    def upload_operation_metadata_path(id)
      "/api/v1/calibre/operations/#{id.to_i}/metadata"
    end
  end
end
