# frozen_string_literal: true

require 'lib_middleware/server/operation_status_updater'

Sidekiq.configure_server do |config|
  config.redis = { url: Config.redis_url }
end

Sidekiq.configure_client do |config|
  config.redis = { url: Config.redis_url }
end

Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    chain.add LibMiddleware::Server::OperationStatusUpdater
  end
end
