# frozen_string_literal: true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'app', 'workers'))
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'app', 'services'))

Bundler.require :default, ENV['APP_ENV']
require 'active_support/all'
require 'shellwords'
require 'tempfile'

application_file_paths = [
  './initializers/**/*.rb',
  '../app/workers/**/*.rb',
  '../app/services/**/*.rb',
  '../app/helpers/**/*.rb'
]

application_file_paths.each do |pattern|
  Dir[File.expand_path(pattern, __dir__)].sort.each(&method(:require))
end
