# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'

ENV['APP_ENV'] ||= 'development'

require File.expand_path('application', __dir__)
