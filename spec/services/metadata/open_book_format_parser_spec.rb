# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Metadata::OpenBookFormatParser do
  subject(:parser) { described_class.new(File.read('spec/fixtures/book.opf')) }

  describe '#title' do
    it { expect(parser.title).to eq('Test Library integration') }
  end

  describe '#description' do
    it { expect(parser.description).to eq('Hello world!') }
  end

  describe '#authors' do
    it { expect(parser.authors).to contain_exactly('Marius Bobin') }
  end

  describe '#published_at' do
    it { expect(parser.published_at).to eq(Date.parse('Wed, 30 Sep 2020')) }
  end

  describe '#tags' do
    it { expect(parser.tags).to contain_exactly('ruby', 'grape') }
  end

  describe '#isbn' do
    it { expect(parser.isbn).to eq('9780974514055') }
  end
end
