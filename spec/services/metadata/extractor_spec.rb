# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Metadata::Extractor do
  let(:download_path) { '/api/v1/files/1/download' }

  subject(:extractor) { described_class.new(download_path) }

  describe '#execute' do
    let(:file_data) { File.read('spec/fixtures/book.epub') }

    before do
      conn = double('conn')
      response = double('response',
                        headers: { 'content-type' => 'application/epub+zip' },
                        body: file_data)

      expect(HTTPUtils).to receive(:download_conn).and_return(conn)
      expect(conn).to receive(:get).with(download_path).and_return(response)
    end

    it 'returns book data' do
      expect(extractor.execute).to match(
        title: 'Test Library integration',
        authors: ['Marius Bobin'],
        description: 'Hello world!',
        isbn: '9780974514055',
        published_at: Date.parse('Wed, 30 Sep 2020'),
        tags: match_array(%w[ruby grape])
      )
    end
  end
end
