# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Metadata::IsbnExtractor do
  subject(:extractor) { described_class.new('9780974514055') }

  describe '#execute' do
    subject(:data) { extractor.execute }

    it 'returns book attributes' do
      is_expected.to match(
        title: "Programming Ruby: The Pragmatic Programmers' Guide",
        description: a_string_starting_with('Ruby is an increasingly popular'),
        authors: ['David Thomas'],
        published_at: Date.parse('Thu, 15 Jan 2004'),
        tags: match_array(%w[Computers General Programming Reference]),
        isbn: '9780974514055'
      )
    end
  end
end
