# frozen_string_literal: true

require 'spec_helper'

RSpec.describe PollingWorker do
  let(:worker) { described_class.new }

  describe '#perform' do
    before do
      expect(worker)
        .to receive(:request_new_jobs)
        .and_return(stubbed_response)
    end

    context 'when there are no jobs available' do
      let(:stubbed_response) { double('response', body: '[]') }

      it 'does not raise errors' do
        expect(worker.perform).to eq([])
      end
    end

    context 'when no worker matches' do
      let(:stubbed_response) do
        double('response', body: [{ data: :none }].to_json)
      end

      it 'enqueues a null object job' do
        expect(NoWorker).to receive(:perform_async).with({ 'data' => 'none' })

        worker.perform
      end
    end

    context 'when a worker matches' do
      let(:stubbed_response) do
        double('response', body: [{ payload: { operation: :convert } }].to_json)
      end

      it 'enqueues a conversion job' do
        expect(ConversionWorker)
          .to receive(:perform_async)
          .with({ 'payload' => { 'operation' => 'convert' } })

        worker.perform
      end
    end
  end
end
