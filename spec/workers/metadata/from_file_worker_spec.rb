# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Metadata::FromFileWorker do
  describe '.matches?' do
    subject { described_class.matches?(payload) }

    context 'when the payload matches' do
      let(:payload) do
        {
          'payload' => {
            'operation' => 'extract_metadata',
            'options' => {
              'source' => 'file'
            }
          }
        }
      end

      it { is_expected.to be_truthy }
    end

    context 'when the payload does not match' do
      let(:payload) do
        {
          'payload' => {
            'operation' => 'extract_metadata',
            'options' => {
              'source' => 'isbn'
            }
          }
        }
      end

      it { is_expected.to be_falsey }
    end
  end

  describe '#perform' do
    let(:payload) do
      {
        'id' => 5,
        'payload' => {
          'options' => {
            'file_url' => '/download/file'
          }
        }
      }
    end

    let(:extractor) do
      double('Metadata::Extractor', execute: { a: :b })
    end

    let(:mocked_conn) do
      double('conn')
    end

    subject(:perform) do
      described_class.new.perform(payload)
    end

    before do
      expect(Metadata::Extractor)
        .to receive(:new)
        .with('/download/file')
        .and_return(extractor)

      expect(HTTPUtils)
        .to receive(:json_conn)
        .and_return(mocked_conn)

      expect(mocked_conn)
        .to receive(:post)
        .with('/api/v1/calibre/operations/5/metadata', '{"metadata":{"a":"b"}}')
        .and_return(true)
    end

    it 'uploads metadata' do
      is_expected.to be_truthy
    end
  end
end
