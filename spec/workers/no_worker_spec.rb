# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoWorker do
  describe '.perform_async' do
    it 'implements perform_async' do
      expect(described_class.perform_async(1)).to be_nil
    end
  end
end
