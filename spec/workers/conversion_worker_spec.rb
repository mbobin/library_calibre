# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ConversionWorker do
  describe '.matches?' do
    subject { described_class.matches?(payload) }

    context 'when the payload matches' do
      let(:payload) do
        {
          'payload' => {
            'operation' => 'convert'
          }
        }
      end

      it { is_expected.to be_truthy }
    end

    context 'when the payload does not match' do
      let(:payload) do
        {
          'payload' => {
            'operation' => 'extract_metadata'
          }
        }
      end

      it { is_expected.to be_falsey }
    end
  end

  describe '#perform' do
    let(:payload) do
      {
        'id' => 5,
        'payload' => {
          'options' => {
            'file_url' => '/download/file',
            'target_format' => 'pdf'
          }
        }
      }
    end

    let(:new_file_path) do
      'new/file/path'
    end

    let(:convertor) do
      double('Convertor', execute: new_file_path)
    end

    let(:uploader) do
      double('Uploader', execute: true)
    end

    subject(:perform) do
      described_class.new.perform(payload)
    end

    before do
      expect(Convertor)
        .to receive(:new)
        .with('/download/file', 'pdf')
        .and_return(convertor)

      expect(Uploader)
        .to receive(:new)
        .with(5, new_file_path, 'pdf')
        .and_return(uploader)

      expect(File)
        .to receive(:exists?)
        .with(new_file_path)
        .and_return(true)

      expect(FileUtils)
        .to receive(:rm)
        .with(new_file_path)
    end

    it 'uploads converted file' do
      is_expected.to be_truthy
    end
  end
end
