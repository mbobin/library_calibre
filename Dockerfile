FROM ruby:2.7

RUN  apt update && \
  apt install -y --no-install-recommends\
    libnss3 \
    libxcomposite1 \
    libxdamage1 \
    libxi6 \
    libxrandr2 \
    libgl1 \
  && \
  apt-get clean && \
  mkdir -p /opt/calibre && \
  wget http://download.calibre-ebook.com/5.9.0/calibre-5.9.0-x86_64.txz && \
  tar -xf calibre-5.9.0-x86_64.txz -C /opt/calibre && \
  rm -rf /tmp/* \
    /var/lib/apt/lists/* \
    /var/tmp/* \
    /calibre-5.9.0-x86_64.txz

ENV APP_PATH /var/apps/library_calibre
ENV APP_ENV production
ENV PATH "$PATH:/opt/calibre"

WORKDIR $APP_PATH
ADD Gemfile $APP_PATH
ADD Gemfile.lock $APP_PATH
RUN gem install bundler -v 2.1.4 && \
  bundle install && \
  adduser --no-create-home --disabled-password --disabled-login --gecos '' librarian
COPY . $APP_PATH

USER librarian

CMD ["bin/sidekiq"]
