# frozen_string_literal: true

class Convertor
  def initialize(file_url, target_format)
    @file_url = file_url
    @target_format = target_format
  end

  def execute
    download_file
    convert_file
    target_file_path
  ensure
    file_path.unlink if file_path&.exist?
  end

  private

  attr_reader :file_url, :target_format, :file_path

  def download_file
    @file_path = Downloader.new(file_url).execute
  end

  def convert_file
    `/usr/bin/env ebook-convert #{Shellwords.escape(file_path)} #{Shellwords.escape(target_file_path)}`
    raise 'Conversion Failed' unless $?.success?
  end

  def target_file_path
    @target_file_path ||= Tempfile.new([SecureRandom.uuid, ".#{target_format}"]).path
  end
end
