# frozen_string_literal: true

module Metadata
  class Extractor
    def initialize(file_url)
      @file_url = file_url
    end

    def execute
      download_file
      data = extract_data
      data.to_h
    ensure
      file_path.unlink if file_path&.exist?
    end

    private

    attr_reader :file_path, :file_url

    def tempfile
      @tempfile ||= ::Tempfile.new
    end

    def extract_data
      safe_file = Shellwords.escape(file_path)
      `/usr/bin/env ebook-meta #{safe_file} --to-opf #{tempfile.path}`
      OpenBookFormatParser.new(tempfile.read)
    ensure
      tempfile.close
      tempfile.unlink
    end

    def download_file
      @file_path = Downloader.new(file_url).execute
    end
  end
end
