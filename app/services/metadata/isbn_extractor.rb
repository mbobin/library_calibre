# frozen_string_literal: true

module Metadata
  class IsbnExtractor
    def initialize(isbn)
      @isbn = isbn
    end

    def execute
      data = extract_data
      data.to_h
    end

    private

    attr_reader :isbn

    def extract_data
      output = `/usr/bin/env fetch-ebook-metadata -i #{Shellwords.escape(isbn)} -o`
      OpenBookFormatParser.new(output)
    end
  end
end
