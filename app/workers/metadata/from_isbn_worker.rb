# frozen_string_literal: true

module Metadata
  class FromIsbnWorker < ApplicationWorker
    def self.matches?(payload)
      payload.dig('payload', 'operation') == 'extract_metadata' &&
        payload.dig('payload', 'options', 'source') == 'isbn'
    end

    def perform(payload)
      isbn     = payload.dig('payload', 'options', 'isbn')
      metadata = Metadata::IsbnExtractor.new(isbn).execute
      path     = Config.upload_operation_metadata_path(payload['id'])
      payload  = { metadata: metadata }.to_json

      HTTPUtils.json_conn.post(path, payload)
    end
  end
end
