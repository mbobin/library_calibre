# frozen_string_literal: true

module Metadata
  class FromFileWorker < ApplicationWorker
    def self.matches?(payload)
      payload.dig('payload', 'operation') == 'extract_metadata' &&
        payload.dig('payload', 'options', 'source') == 'file'
    end

    def perform(payload)
      file_url = payload.dig('payload', 'options', 'file_url')
      metadata = Metadata::Extractor.new(file_url).execute
      path     = Config.upload_operation_metadata_path(payload['id'])
      payload  = { metadata: metadata }.to_json

      HTTPUtils.json_conn.post(path, payload)
    end
  end
end
