# frozen_string_literal: true

class ApplicationWorker
  include Sidekiq::Worker

  def executes_operations?
    true
  end
end
