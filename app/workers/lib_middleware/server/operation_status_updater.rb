# frozen_string_literal: true

module LibMiddleware
  module Server
    class OperationStatusUpdater
      # @param [Object] worker the worker instance
      # @param [Hash] job the full job payload
      #   * @see https://github.com/mperham/sidekiq/wiki/Job-Format
      # @param [String] queue the name of the queue the job was pulled from
      # @yield the next middleware in the chain or worker `perform` method
      # @return [Void]
      def call(worker, job, _queue)
        yield
        mark_operation_as_successful(worker, job)
      rescue StandardError => e
        mark_operation_as_failed(worker, job, e)
        puts e.message
      end

      def mark_operation_as_successful(worker, job)
        return unless worker.executes_operations?

        payload = { status: 'success' }
        update_job_status(job, payload)
      end

      def mark_operation_as_failed(worker, job, ex)
        return unless worker.executes_operations?

        payload = {
          status: 'failed',
          result: {
            fail_reason: ex.message,
            backtrace: ex.backtrace
          }
        }

        update_job_status(job, payload)
      end

      def update_job_status(job, payload)
        operation_id = job.dig('args', 0, 'id').to_i
        update_path  = Config.update_operation_path(operation_id)

        HTTPUtils
          .json_conn
          .post(update_path, payload.to_json)
      rescue StandardError => e
        puts e.message
      end
    end
  end
end
