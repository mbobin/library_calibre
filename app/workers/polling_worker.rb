# frozen_string_literal: true

class PollingWorker < ApplicationWorker
  AVAILABLE_WORKERS = [
    Metadata::FromFileWorker,
    Metadata::FromIsbnWorker,
    ConversionWorker
  ].freeze

  def executes_operations?
    false
  end

  def perform
    response = request_new_jobs

    JSON.parse(response.body).each do |job_info|
      worker_class = AVAILABLE_WORKERS.find(-> { NoWorker }) do |klass|
        klass.matches?(job_info)
      end

      worker_class.perform_async(job_info)
    end
  end

  private

  def request_new_jobs
    HTTPUtils
      .json_conn
      .post(Config.request_operations_path)
  end
end
