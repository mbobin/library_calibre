# frozen_string_literal: true

class ConversionWorker < ApplicationWorker
  def self.matches?(payload)
    payload.dig('payload', 'operation') == 'convert'
  end

  def perform(payload)
    file_url, target_format = payload
                              .dig('payload', 'options')
                              .values_at('file_url', 'target_format')

    file_path = Convertor
                .new(file_url, target_format)
                .execute

    Uploader
      .new(payload['id'], file_path, target_format)
      .execute
  ensure
    FileUtils.rm(file_path) if File.exists?(file_path.to_s)
  end
end
