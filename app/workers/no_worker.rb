# frozen_string_literal: true

class NoWorker
  def self.perform_async(payload)
    puts "No worker matches the payload: #{payload}"
  end
end
