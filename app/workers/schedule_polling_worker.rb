# frozen_string_literal: true

class SchedulePollingWorker < ApplicationWorker
  def executes_operations?
    false
  end

  def perform
    [1, 10, 20, 30, 40, 50].each do |offset|
      PollingWorker.perform_in(offset.seconds.from_now)
    end
  end
end
