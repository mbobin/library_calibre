# frozen_string_literal: true

class Uploader
  def initialize(operation_id, file_path, extension)
    @operation_id  = operation_id
    @file_path     = file_path
    @extension = extension
  end

  def execute
    File.open(file_path) do |io|
      payload = {
        document: Faraday::FilePart.new(io, mime_type, filename)
      }

      HTTPUtils.upload_conn.post(upload_path, payload)
    end
  end

  private

  attr_reader :operation_id, :file_path, :extension

  def upload_path
    Config.upload_operation_files_path(operation_id)
  end

  def mime_type
    MIME::Types
      .find { |type| type.preferred_extension == extension }
      &.content_type
  end

  def filename
    File.basename(file_path)
  end
end
