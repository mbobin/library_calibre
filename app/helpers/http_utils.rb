# frozen_string_literal: true

class HTTPUtils
  class << self
    def json_conn
      Faraday.new(Config.api_host) do |f|
        f.headers = { 'Content-Type' => 'application/json' }
        f.basic_auth Config.api_username, Config.api_password
        f.ssl.ca_file = Config.ca_file_path
        f.adapter Faraday.default_adapter
      end
    end

    def download_conn
      Faraday.new(Config.api_host) do |f|
        f.basic_auth Config.api_username, Config.api_password
        f.use FaradayMiddleware::FollowRedirects, limit: 2
        f.ssl.ca_file = Config.ca_file_path
        f.adapter Faraday.default_adapter
      end
    end

    def upload_conn
      Faraday.new(Config.api_host) do |f|
        f.basic_auth Config.api_username, Config.api_password
        f.request :multipart
        f.request :url_encoded
        f.ssl.ca_file = Config.ca_file_path
        f.adapter Faraday.default_adapter
      end
    end
  end
end
