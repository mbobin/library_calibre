# frozen_string_literal: true

class Downloader
  def initialize(file_url)
    @file_url = file_url
  end

  def execute
    response  = HTTPUtils.download_conn.get(@file_url)
    extension = find_extension(response)
    file_path = Pathname("/tmp/#{SecureRandom.uuid}.#{extension}")

    IO.binwrite(file_path, response.body)
    file_path
  end

  private

  def find_extension(response)
    long_type = response.headers['content-type']
    MIME::Types[long_type].first.preferred_extension
  end
end
